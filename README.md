# primegaming-loot-autocollect

Automatically collect Amazon Prime Gaming loots.

## Getting started

- Create an environment file from the example file : `cp .env.example .env`
- Fill the `.env` environment file
  - Fill `LOOT_ID` with the loot ID located at the end of the following URL : <https://gaming.amazon.com/loot/>
  - Fill `USERNAME` & `PASSWORD` with your Amazon credentials
  - Optionally fill `TOTP` with your Amazon TOTP 2FA key
- Run the main script : `yarn start`