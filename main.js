const run = require('./mod.js');

run({
    lootId: process.env.LOOT_ID,
    email: process.env.EMAIL,
    password: process.env.PASSWORD,
    totpKey: process.env.TOTP,
    onStep: (index, length) => console.log(`Running step ${index+1}/${length}`)
}).then(() => console.log('Done')).catch(console.error);