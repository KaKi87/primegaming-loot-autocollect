const
    puppeteer = require('puppeteer-extra'),
    totp = require('totp-generator');

puppeteer.use(require('puppeteer-extra-plugin-stealth')());

module.exports = async ({
    lootId,
    email,
    password,
    totpKey,
    onStep
}) => {
    let error;
    const
        browser = await puppeteer.launch({
            headless: false,
            defaultViewport: null,
            args: ['--start-maximized']
        }),
        page = await (await browser.createIncognitoBrowserContext()).newPage();
    await page.goto(`https://gaming.amazon.com/loot/${lootId}`);
    const steps = [
        async () => await (await page.waitForSelector('[data-a-target="sign-in-button"]')).click(),
        async () => await (await page.waitForSelector('input[type="email"]')).type(email),
        async () => await page.type('input[type="password"]', password),
        async () => await page.click('input[type="submit"]'),
        async () => await (await page.waitForSelector('input[type="tel"]')).type(totp(totpKey)),
        async () => await page.click('input[type="submit"]'),
        async () => await (await page.waitForSelector('[data-a-target="AvailableButton"]')).click(),
        async () => await page.waitForSelector('.gms-success-modal-container')
    ].map((step, index, { length }) => async () => {
        if(onStep)
            onStep(index, length);
        await step();
    });
    for(let i = 0; i < steps.length; i++){
        const
            currentStep = steps[i],
            previousStep = steps[i-1];
        try {
            await currentStep();
        }
        catch {
            if(previousStep)
                try { await previousStep(); } catch {}
            try {
                await currentStep();
            }
            catch(_error){
                error = _error;
                break;
            }
        }
    }
    await browser.close();
    if(error)
        throw error;
};